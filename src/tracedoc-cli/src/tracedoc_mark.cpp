#include <iostream>
#include <vector>

#include <cxxopts.hpp>
#include <opencv2/opencv.hpp>
#include <annotations.h>

using namespace std;
using namespace cv;


const string pref = "--> ";

static void onMouse(int event, int x, int y, int flags, void * param);


class CPerformer
{
    CAnnotator annotator;
    Mat im;
    Mat original;
    vector<Point> points;
public:

    explicit CPerformer(const string & fname) {
        original = imread(fname);

        if (original.empty())
            throw runtime_error("failed to read image.");

        annotator = CAnnotator(fname);

        annotator.GetPoints(points);

    };

    void PlotMarker(int x, int y)
    {
        drawMarker(im, Point(x,y), Scalar(0,0,255), MARKER_CROSS, 40, 2);
    }

    void ProcessMouseDown(int x, int y)
    {
        PlotMarker(x, y);
        points.emplace_back(Point(x, y));
        imshow("TraceDoc", im);
    }

    void ShowPoints()
    {
        im = original.clone();

        for (const auto &it : points)
            drawMarker(im, it, Scalar(0,0,255), MARKER_CROSS, 40, 2);

        imshow("TraceDoc", im);
    }

    bool Start()
    {
        namedWindow("TraceDoc", CV_WINDOW_NORMAL);

        ShowPoints();
        setMouseCallback("TraceDoc", onMouse, (void *) this);

        return true;
    }

    bool ProcessKey(char c)
    {
        switch(c)
        {
            case 27:
                return false;
            case 115: // "s"
                annotator.SetPoints(points);
                annotator.Save();
                return true;
            case 99: // "c"
                points.clear();
                ShowPoints();
                return true;
            default:
                return true;
        }
    }
};

static void onMouse(int event, int x, int y, int flags, void * param) {
    if (event == EVENT_LBUTTONDOWN)
    {
        ((CPerformer *) param)->ProcessMouseDown(x, y);

    }
}


int main(int argc, char *argv[]) {
    const string title = "TraceDoc: Markup.";

    cout << endl << endl;

    try {
        cxxopts::Options options("tracedoc_markup", title);

        options.add_options()
                ("f,file", "image file name", cxxopts::value<std::string>());

        auto result = options.parse(argc, argv);

        if (result.count("help") || !result.count("file")) {
            cout << options.help({""}) << endl;
            exit(0);
        }

        cout << title << endl << endl;

        CPerformer performer(result["file"].as<string>());

        performer.Start();

        for(;;)
        {
            auto c = waitKey();
            if(!performer.ProcessKey(c))
                break;
        }

    }
    catch (const cxxopts::OptionException &e) {
        std::cout << pref << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
    catch (const std::exception &e) {
        std::cout << "error: " << e.what() << std::endl;
        exit(1);
    }


    return 0;
}