#include <iostream>
#include <vector>

#include <cxxopts.hpp>
#include <opencv2/opencv.hpp>
#include <annotations.h>

#include <tracedoc/segmentation.h>
#include <tracedoc/enhancer.h>

using namespace std;
using namespace cv;

const string pref = "--> ";

class CPerformer
{
    CAnnotator annotator;
    Mat source;

    CTraceDocSegmentation segmentation;
    CTraceDocEnhancer enhancer;

public:

    explicit CPerformer(const string & sourcename)
    {
        source = imread(sourcename);

        if (source.empty())
            throw runtime_error("failed to read original.");

        source = imread(sourcename);

        if (source.empty())
            throw runtime_error("failed to read source.");

        annotator = CAnnotator(sourcename);
        
    };

    bool Process()
    {
        vector<Rect> words;
        
        Mat processed;
        
        enhancer.Process(source, processed);

        segmentation.GetTextRegions(processed, words);
        
        annotator.SetTextRegions(words);
        
        annotator.Save();

        return true;
    }

};


int main(int argc, char *argv[]) {
    const string title = "TraceDoc: Retrieve.";

    cout << endl << endl;

    try {
        cxxopts::Options options("tracedoc_retrieve", title);

        options.add_options()
                ("s,source", "source file name", cxxopts::value<std::string>());

        auto result = options.parse(argc, argv);

        if (result.count("help") || !result.count("source"))
        {
            cout << options.help({""}) << endl;
            exit(0);
        }

        cout << title << endl << endl;

        CPerformer performer(result["source"].as<string>());

        performer.Process();

    }
    catch (const cxxopts::OptionException &e) {
        std::cout << pref << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
    catch (const std::exception &e) {
        std::cout << "error: " << e.what() << std::endl;
        exit(1);
    }


    return 0;
}