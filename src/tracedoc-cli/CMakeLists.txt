cmake_minimum_required(VERSION 3.9)
project(tracedocutils VERSION 0.2 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)

find_package(OpenCV REQUIRED)

set(PACKAGE_NAME
        tracedoc-cli
        )

set(LIBRARY_NAME
        tracedoc
        )

set(PACKAGE_SOURCE_DIR
        src
        )

set(PACKAGE_HEADERS_DIR
        include
        )

set(PACKAGE_HEADERS
        ${PACKAGE_HEADERS_DIR}/cxxopts.hpp
        )

include_directories(
        ${LIBRARY_HEADERS}
        ${PACKAGE_HEADERS_DIR}
        ${OpenCV_INCLUDE_DIRS}
)

add_executable(tracedoc_classify
        src/tracedoc_classify.cpp ${PACKAGE_HEADERS})

add_executable(tracedoc_mark
        src/tracedoc_mark.cpp ${PACKAGE_HEADERS})

add_executable(tracedoc_restore
        src/tracedoc_restore.cpp ${PACKAGE_HEADERS})

add_executable(tracedoc_retrieve
        src/tracedoc_retrieve.cpp ${PACKAGE_HEADERS})

target_link_libraries(tracedoc_classify
        ${LIBRARY_NAME} ${OpenCV_LIBS})

target_link_libraries(tracedoc_mark
        ${LIBRARY_NAME} ${OpenCV_LIBS})

target_link_libraries(tracedoc_restore
        ${LIBRARY_NAME} ${OpenCV_LIBS})

target_link_libraries(tracedoc_retrieve
        ${LIBRARY_NAME} ${OpenCV_LIBS})
