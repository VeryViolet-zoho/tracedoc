#ifndef TRACEDOC_ANNOTATIONS_H
#define TRACEDOC_ANNOTATIONS_H

#include <string>
#include <iostream>
#include <fstream>
#include <streambuf>

#include <opencv2/opencv.hpp>

#include <json.hpp>

using namespace std;
using namespace cv;
using json::JSON;

const string postfix = ".annotation";
const string default_annotation = "{\"points\": [], \"text\": [], \"images\": []}";

class CAnnotator
{
    string filename;
    JSON object;
public:
    CAnnotator(string fname="") {
        if(fname.empty())
            return;
        filename = fname + postfix;
        Load();
    };

    ~CAnnotator() = default;

    void Create() {
        string contents = default_annotation;
        object = JSON::Load(contents);
        Save();
    }

    void Load() {
        ifstream in(filename, std::ios::in | std::ios::binary);
        string contents;

        if (in)
        {
            in.seekg(0, std::ios::end);

            if(in.tellg() == 0)
            {
                Create();
                return;
            }
            else
            {
                in.seekg(0, std::ios::beg);

                std::stringstream buffer;
                buffer << in.rdbuf();

                contents= buffer.str();
            }
        }
        else {
            Create();
            return;
        }

        object = JSON::Load(contents);
    }

    bool Save() {
        ofstream out(filename, std::ios::out | std::ios::binary);

        if (out)
            out << object;
    }

    unsigned long GetPoints(vector<Point> & lst) {
        lst.clear();

        for(int i=0; i<object["points"].size(); i++)
            lst.emplace_back((int) object["points"][i][0].ToInt(), (int) object["points"][i][1].ToInt());

        return lst.size();
    }

    void SetPoints(const vector<Point> & lst) {
        JSON points("[]");

        for (const auto &it : lst)
        {
            JSON point("[]");
            point.append(it.x);
            point.append(it.y);
            points.append(point);

        }

        object["points"] = points;
    }

    int GetTextRegions(vector<Rect> & lst) {
        return 0;
    }

    void SetTextRegions(const vector<Rect> & lst) {

    }

    int GetImageRegions(vector<Rect> & lst) {
        return 0;
    }

    void SetImageRegions(const vector<Rect> & lst) {

    }
};

#endif //TRACEDOC_ANNOTATIONS_H
