#ifndef LIBTRACEDOC_SEGMENTATION_H
#define LIBTRACEDOC_SEGMENTATION_H

#include <vector>
#include <iostream>

#include <opencv2/text.hpp>
#include <opencv2/opencv.hpp>


using namespace cv;
using namespace cv::text;
using namespace std;

enum TRACEDOC_TEXT_METHOD
{
    EXTREMAL_REGIONS,
    CONTOURS
};

#define TRACEDOC_MAX_AREA_DEVIATION 10
#define TRACEDOC_MAX_RATIO 30


class CTraceDocSegmentation
{
public:
    CTraceDocSegmentation();
    ~CTraceDocSegmentation();
    bool GetTextRegions(Mat & src, vector<Rect> & target, TRACEDOC_TEXT_METHOD method = CONTOURS);
    bool GetImageRegions(Mat & src, vector<Rect> & target);
private:
    bool GetTextRegionsER(Mat & src, vector<Rect> & target);
    bool GetTextRegionsContours(Mat & src, vector<Rect> & target);
};

#endif