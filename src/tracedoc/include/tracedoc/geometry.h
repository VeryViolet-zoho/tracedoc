#ifndef LIBTRACEDOC_GEOMETRY_H
#define LIBTRACEDOC_GEOMETRY_H

#include <vector>
#include <opencv2/opencv.hpp>

#define MINIMUM_POINTS_REQUIRED 4

using namespace cv;
using namespace std;

class CTraceDocGeometry
{
public:
    CTraceDocGeometry();
    ~CTraceDocGeometry();
    Mat Process(Mat &source, vector<Point> &source_points,
                Mat &original, vector<Point> &original_points);
};


#endif