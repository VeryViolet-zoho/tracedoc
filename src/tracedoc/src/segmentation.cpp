#include "segmentation.h"

CTraceDocSegmentation::~CTraceDocSegmentation() {

}

CTraceDocSegmentation::CTraceDocSegmentation() {

}

bool CTraceDocSegmentation::GetTextRegions(Mat &src, vector<Rect> & target, TRACEDOC_TEXT_METHOD method)
{
    switch(method)
    {
        case EXTREMAL_REGIONS:
            return GetTextRegionsER(src, target);
        case CONTOURS:
            return GetTextRegionsContours(src, target);
    }

    return true;
}

bool CTraceDocSegmentation::GetImageRegions(Mat &src, vector<Rect> & target)
{
    return true;
}

bool CTraceDocSegmentation::GetTextRegionsContours(Mat &src, vector<Rect> &target)
{
    Mat grad;
    Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));
    morphologyEx(src, grad, MORPH_GRADIENT, morphKernel);

    Mat thr;
    threshold(grad, thr, 128, 255, THRESH_BINARY);

    Mat denoised;
    morphKernel = getStructuringElement(MORPH_RECT, Size(5, 5));
    morphologyEx(thr, denoised, MORPH_OPEN, morphKernel, Point(-1, -1), 2);

    Mat connected;
    morphKernel = getStructuringElement(MORPH_RECT, Size(9, 1));
    morphologyEx(thr, connected, MORPH_CLOSE, morphKernel);

    Mat mask = Mat::zeros(connected.size(), CV_8UC1);
    Mat ready;

    connected.convertTo(ready, CV_8UC1);

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;

    cv::findContours(ready, contours, CV_RETR_EXTERNAL,CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

    target.clear();

    float mean = 0, std = 0;
    vector<Rect> allrects;

    for(const vector<Point> & contour: contours)
        allrects.push_back(boundingRect(contour));


    for(const Rect & r: allrects)
        mean += r.area();

    mean /= contours.size();

    for(const Rect & r: allrects)
    {
        if(r.area() < mean*TRACEDOC_MAX_AREA_DEVIATION and r.area() > mean/TRACEDOC_MAX_AREA_DEVIATION and
                max(r.width, r.height)/min(r.width, r.height) < TRACEDOC_MAX_RATIO)
            target.push_back(r);
    }

    return true;

}

bool CTraceDocSegmentation::GetTextRegionsER(Mat &src, vector<Rect> &target)
{
    Mat gray;
    cvtColor(src, gray, CV_BGR2GRAY);
    // morphological gradient
    Mat grad;
    Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));
    morphologyEx(gray, grad, MORPH_GRADIENT, morphKernel);
    // binarize
    Mat bw;
    threshold(grad, bw, 0.0, 255.0, THRESH_BINARY | THRESH_OTSU);

    Mat connected;
//    morphKernel = getStructuringElement(MORPH_RECT, Size(9, 1));
    morphKernel = getStructuringElement(MORPH_RECT, Size(18, 1));
    morphologyEx(bw, connected, MORPH_CLOSE, morphKernel);

    bitwise_not(connected, connected);
    cvtColor(connected, connected, CV_GRAY2BGR);

    imwrite("totest.jpg", connected);

    vector<Mat> channels;
    computeNMChannels(connected, channels);

    int cn = (int)channels.size();
    // Append negative channels to detect ER- (bright regions over dark background)
    for (int c = 0; c < cn-1; c++)
        channels.push_back(255-channels[c]);

    Ptr<ERFilter> er_filter1 = createERFilterNM1(loadClassifierNM1("trained_classifierNM1.xml"),16,0.00015f,0.13f,0.2f,true,0.1f);
    Ptr<ERFilter> er_filter2 = createERFilterNM2(loadClassifierNM2("trained_classifierNM2.xml"),0.5);

    vector<vector<ERStat> > regions(channels.size());

    for (int c=0; c<(int)channels.size(); c++)
    {
        er_filter1->run(channels[c], regions[c]);
        er_filter2->run(channels[c], regions[c]);
    }

    vector< vector<Vec2i> > region_groups;
    erGrouping(src, channels, regions, region_groups, target, ERGROUPING_ORIENTATION_HORIZ);

    er_filter1.release();
    er_filter2.release();
    regions.clear();
    return false;
}
