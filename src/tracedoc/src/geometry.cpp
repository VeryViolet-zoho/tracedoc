#include "geometry.h"

#include <iostream>

CTraceDocGeometry::CTraceDocGeometry()
{

}

CTraceDocGeometry::~CTraceDocGeometry()
{

}

Mat CTraceDocGeometry::Process(Mat &source, vector<Point> &source_points, Mat &original, vector<Point> &original_points)
{
    if (source_points.size() != original_points.size())
        throw invalid_argument("point count must be the same.");

    Point2f * src_points = new Point2f[source_points.size()];
    Point2f * dst_points = new Point2f[original_points.size()];
    
    for(int i=0; i< source_points.size(); i++)
    {
        src_points[i] = source_points[i];
        dst_points[i] = original_points[i];
    }

    auto trans = getPerspectiveTransform(src_points, dst_points);

    delete[] src_points;
    delete[] dst_points;

    Mat target = Mat(original.size(), original.type());

    warpPerspective(source, target, trans, target.size());

    return target;
}
